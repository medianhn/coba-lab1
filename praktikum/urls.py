from django.conf.urls import url, include
from django.contrib import admin
import lab_1.urls as lab_1
import lab_2.urls as lab_2
import lab_3.urls as lab_3
import lab_4.urls as lab_4
import lab_5.urls as lab_5
from lab_1.views import index as index_lab1
from lab_2.views import index as index_baru

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^lab-1/', include(lab_1,namespace='lab-1')),
    url(r'^lab-2/', include(lab_2,namespace='lab-2')),
    url(r'^lab-3/', include(lab_3,namespace='lab-3')),
    url(r'^lab-4/', include(lab_4,namespace='lab-4')),
    url(r'^lab-5/', include(lab_5,namespace='lab-5')),
    url(r'^$', index_baru, name='index')
]
